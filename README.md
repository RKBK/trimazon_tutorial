# Synchronizing the S3 bucket and setting up the scripts

1. Open a terminal window
1. install aws-cli python package

    ```bash
    $ pip install --upgrade --user awscli
    ```
1. Configure amazon web services. 
    You will need an AWS access Key ID and an AWS Secret Access Key, 
    both of which you will get from your administrator.
    ```bash
    $ aws configure
    ```
    1. Set AWS Access Key ID to the given Access Key
    2. Set the AWS Secret Access Key to the given Secret Access Key
    3. Set the Default region name to us-east-1
    4. Just press `Enter` when it asks you about "default output format" (=Leave default output format as it is)
1. Create the folder for your local matr.io mirror:

    ```bash
    $  mkdir ~/matr.io
    ```
1. Sync the bucket on Amazon S3 to your new matr.io folder.

    ``` bash
    $ aws s3 sync s3://matr.io ~/matr.io --exclude "*/calc.save/*"
    ```
1. Open ~/.bashrc (or similar) in a text editor

1. Make the matr.io submission and sync environment variables active when
    opening a terminal by adding the following to the .bashrc (or similar):

	``` 
    export TRI_PATH=~/matr.io
    source $TRI_PATH/env/init.sh
    ```
1. Go back to the terminal and enter the matr.io directory you created

    ```bash
    $ cd ~/matr.io
    ```
1. Make the TRI submission and sync scripts executable

    ```bash
    $ chmod +x $TRI_PATH/bin/*
    ```
1. Copy the ssh access key to ~/.ssh, which will allow you to log into a running node to observe its execution:

    ```bash
	$ cp $TRI_PATH/simulation/monitor.pem ~/.ssh
	$ chmod 400 ~/.ssh/monitor.pem
    ```
1. Add the following to ~/.ssh/config:

    ```
    ## Login AWS Cloud ##
    Host ec2-*
      User ec2-user
      IdentityFile ~/.ssh/monitor.pem
    ```
    Now you're ready to submit calculations!

# Submitting calculations and syncing data
These are the two key commands you will use:

1. `trisub` (for submitting jobs)
2. `trisync` (for synchronizing data back to your computer)

In the next section, you'll see how they work.

# Running your first calculations
At this point, a local copy of the s3 bucket has been created. It will contain one folder called "model". Under this folder is where all models you create or use will be found. There will also be a folder called simulation, which will contain all finished calculation you or others have performed. Everything in simulation is frozen and cannot be changed or deleted. It is saved forever.

We will exemplify how to run a calculation using the first version of the Quantum Espresso Docker image. We will

1. Run a total energy calculation for a structure *imported* from materialsproject, and watch the execution on AWS.
1. Add *measurements* to our model
1. *Parametrize* the model 

Let's get started. 

## A simple total energy calculation for a structure from materialsproject

1. Create your own directory, and a subfolder called tutorial

    ```bash
	$ mkdir -p ~/matr.io/model/qe/1/users/$USER/tutorial
    ```
1. Go to the folder, and create 3 subfolders, and enter the first one

    ```bash
     $ cd ~/matr.io/model/qe/1/users/$USER/tutorial
     $ mkdir -p firstmodel/_1
     $ mkdir -p modelwithmeasurements/_1
	 $ mkdir -p parametrizedmodel/_1
	 $ cd firstmodel/_1
    ```
	The _1 indicate the *first revision* of each folder. Why do we need revisions? Once you submit a directory, it will be frozen, so if you want to change something in your model, you need to make a new version of it. *This guarantees full provenance*. Revisions are indicated with and underline followed by a number. *Because of this, underlines are not allowed in any folder name*.
	
1. The script should be called *model.py*. Here's the example input script to get you started:

	model.py
	```python
	#!/usr/bin/env python
	
	from ase.io import read
	from ase.units import Rydberg as Ry
	from espresso import espresso

	atoms = read('mp.cif')

	# Recommended cutoffs for GBRV are
	# PW: 40 Ry
	# density: 200 Ry (dual = 5)
	cutoff = 40
	dual = 5

	filename_suffix = '_dual-%s_cutoff-%s' %(dual, cutoff)
	calc = espresso(pw=cutoff*Ry,
		            dw=dual*cutoff*Ry,
					xc='beef',
					kpts=(2,2,2),
					outdir='cell' + filename_suffix,
					atoms=atoms)
    e = atoms.get_potential_energy()
	calc.get_final_structure().write('dual-%s_cutoff-%s-out.traj' %(dual, cutoff))

	```
	Notice we are reading something called 'mp.cif'. This is an example of using an import from materialsproject, through *imports.csv*. *imports.csv* is a file, where you write what structures you want to import into the folder when running. In this case, imports.csv looks like this:
	
	imports.csv
	```
	mp.cif mp-1000
	```
	
1. Go ahead and create the imports.csv file.
	
	You can also just create and add e.g. traj files manually, but this is faster. In the future, you will also be able to import structures from other folders and other services this way, as well as chain jobs together.

1. The script can now be submitted to AWS by running

    ```bash
	$ trisub
    ```
    This will submit the job (on the test queue, with a default of 1 core and 800 MB RAM). Running the command will also create a link to the corresponding
    simulation folder. That's where all your results will end up. 

1. In the future, there will be a simple command for you to check the job status.

1. You can now run trisync to sync the files from s3, and look in the simulation folder to see the files

    ```bash
	$ trisync
	$ ls simulation
     ```
 
1. If the job has started and is still running, you can connect to the running docker instance on Amazon by running
    ```bash
	$ source simulation/monitor.sh
    ```
    You can then follow the job execution as it happens. Your job will be running inside a Docker image, in the working directory /media/scratch. 
1. Once the job completes, running `trisync` again will sync all the (small) data to your computer.

If it turns out that you're not happy with your revision after you've run it, you can manually create a new revision (_2, _3, etc), cd into it, copy the files over, make them writable and change them. Alternatively, you can just run

``` bash
$ trinewrev.py
```
to create a new revision (with all your input files), that you then can cd to.


## Running calculations: trisub options

Before we continue, let's take a look at the options you can provide for `trisub`:

- `m`: the name of the model (if not model.py)
- `p`: parameters for the model (more about this later)
- `d`: dependencies for the model
- `c`: number of vCPUs to run on (default is 1)
- `r`: RAM for the job (in MB, 800 is default)
- `q`: which queue to use (test or spot, test is default)

An example for running a model with 8 vCPUs and 4000 MB or RAM is
```bash
$ trisub -c 8 -m 4000
```

## Making measurements 
To enable measurements, we have to import the measurements library and attach it to the calculation. Currently, for QE, we only use the `.attach` method of geometry optimizers in ASE. With GPAW, you can do the same on the SCF iteration level. In the future, we'll support SCF-level measurements in QE as well.

1. Go to the second folder you created (`model_with_measurements`), and its first revision
1. Copy over model.py and imports.csv to the new folder
1. Make the files writable:

   ```bash
   $ chmod 777 model.py
   $ chmod 777 imports.csv
   ```

1. Add `import measure` to your model.py
1. Convert the script into a geometry optimization
1. Create and attach the measurements object

   ```python
   measurement_obj = measure()
   qn.attach(measurement_obj, interval=1)
   ```

1. Run the calculation on 8 cores with 4000 MB RAM.
1. Take a look at what data was stored

## Parametrizing your model

What if the script for relaxing mp-1000 would only have to be written once, and one could just run it again with new options (e.g. another XC functional, a higher cutoff)? To achieve this, let's *parametrize* our model. 

The machinery for doing this is simple: any hash followed by a number will be interpreted as a parameter. Let's say you want users to be able to run the relaxation with another cutoff. The only thing you have to do is replace

```python
cutoffs = 40
```

with
```python
cutoffs = #1
```

Parameters are given to your model thorugh trisub. In this case, to run exactly the same calculation in our parametrized model, you'd do

```bash
$ trisub -p /50
```

Multiple parameters are evaluated in order, from left to right, and separated by /. So if your model had 2 parameters (#1 being cutoff and #2 being the XC functional, let's say) the submission command would be

``` bash
$ trisub -p /50/beef
```

Let's give this a try.

1. Go to the first revision of the third folder you created (`parametrized model/_1`)
1. Add a few parameters to your model
1. Run trisub a few times with different options

# Future functionality

- Dependencies: enabling a series of models to be run automatically to generate a certain end result
- Automatic database generation using measurements
- Shared models
- Secret folders
- Tagging of calculations
- Generating new docker images through trimazon
- Concurrent usage by multiple research groups
- "Verified" or "Standard" models (e.g. "the SUNCAT recommended chemisorption model")
